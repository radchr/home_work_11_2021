const express = require('express')
const path = require('path')
const app = express()
const axios = require('axios');
const url = 'https://dog.ceo/api/breeds/list/all'

let dataDog = ''


// Make a request for a user with a given ID
app.use(express.static(path.resolve(__dirname, 'static')))
app.set('view engine', 'ejs')

axios.get(url)
.then(function (response) {
  // handle success
  dataDog = JSON.stringify(response.data.message);
})
.catch(function (error) {
  // handle error
  console.log(error);
})
.then(function () {
  // always executed
});



  app.get('/', (req, res) =>{
  
    res.render('index', {h1:`hello ${dataDog}`})
  })
app.listen(3000, () =>{
  console.log("Server start ...")
})