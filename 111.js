const express = require('express')
const fs = require("fs");
const path = require('path')
const app = express()
const axios = require('axios');
const url = 'https://dog.ceo/api/breeds/list/all'

//////// ------  роутинг на директорию static -- или ejs
app.use(express.static(path.resolve(__dirname, 'static')))
app.set('view engine', 'ejs')
//  --- переменные
const fetchArr = [];
const listDog = [];
const dogPic =[];
let dogPicLink = [];
let text = '';

// делаем 10 запросов
for (let i = 0; i < 10; i++){
  fetchArr.push(axios.get(url))
}
// console.log(fetchArr)

Promise.all(fetchArr)
.then((r) => {
  const tempArr = r.map((el) => {
    return Object.keys(el.data.message)
  })
  // console.log(tempArr[1])
  tempArr[1].forEach(element => {
    listDog.push(`https://dog.ceo/api/breed/${element}/images/random`)
  });
  // console.log(listDog.length)

  listDog.forEach(element => {
    dogPic.push(axios.get(element))
  });

// console.log(dogPic)
Promise.all(dogPic).
then((r) => {
   dogPicLink = r.map((el) => {
    // console.log(el.data.message)  
    return el.data.message
    
  })
  // console.log(dogPicLink)  

  dogPicLink.forEach((element, index) => {
    text += `<div> Dog ${index} ==== <img src=${element} alt="альтернативный текст"> </div>`
  });
  // console.log(text)
  fs.writeFileSync(path.resolve(__dirname, 'static', 'index.html'), text); 
  //===================================================================
  ///  - открывается на 3000 порту -- файл index.html -- dir --static
})
} )
.catch( (err)=> {
  console.log(err.stack)
})

  app.get('/', (req, res) =>{
  
  res.render('index', {h1:text})
  })
///       не работает почему то


app.listen(3000, () =>{
  console.log("Server start ...")
})

